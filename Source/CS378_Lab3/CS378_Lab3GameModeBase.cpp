// Copyright Epic Games, Inc. All Rights Reserved.


#include "CS378_Lab3GameModeBase.h"
#include "WorldPawn.h"

ACS378_Lab3GameModeBase::ACS378_Lab3GameModeBase() {
	// set default pawn class to our character class
	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Class'/Game/Blueprint/WorldPawnBP.WorldPawnBP_C'"));

	if (pawnBPClass.Object) {
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		DefaultPawnClass = AWorldPawn::StaticClass();
	}
}