// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ResponseActor.generated.h"

class ATriggerActor;

UCLASS()
class CS378_LAB3_API AResponseActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AResponseActor();

	FORCEINLINE class UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
	void RespondToTrigger();

	UPROPERTY(Category = Mesh, EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		ATriggerActor* TriggerActor;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
